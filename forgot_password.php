<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();
 
if (login_check($mysqli) == true) {
   $logged = 'in';
   session_destroy();
   header('Location: index.php');
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css?jNiMS7GqGhSVkYvtqohaMSAFspb3KXnG38dZrIsb" rel="stylesheet" type="text/css">
    <link href="css/AdminLTE.min.css?jNiMS7GqGhSVkYvtqohaMSAFspb3KXnG38dZrIsb" rel="stylesheet" type="text/css">
    <title>Forgot Password- Guest Karma</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/style.css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Lato:100,300,400,700,900|Montserrat:100,200,300,400,500,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script src="js/sha512.js" type="text/JavaScript">
    </script>
    <script src="js/forms.js" type="text/JavaScript">
    </script>

    <?php
    $modal_padding = "5px";
    ?>

    <style>

        #white-background {
            display: none;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0px;
            left: 0px;
            background-color: #fefefe;
            opacity: 0.7;
            z-index: 9999;
        }

        #dlgbox {
            display: none;
            position: fixed;
            width: 480px;
            z-index: 9999;
            border-radius: 5px;
            background-color: #7c7d7e;
        }

        #dlg-header {
            background-color: black;
            color: white;
            font-size: 20px;
            padding: 10px;
            margin: <?= $modal_padding; ?> <?= $modal_padding; ?> 0px <?= $modal_padding; ?>;
        }

        #dlg-body {
            background-color: white;
            color: black;
            font-size: 14px;
            padding: 10px;
            margin: 0px <?= $modal_padding; ?> 0px <?= $modal_padding; ?>;
            overflow-y: scroll;
            max-height: 300px;
        }

        #dlg-footer {
            background-color: #f2f2f2;
            text-align: right;
            padding: 10px;
            margin: 0px <?= $modal_padding; ?> <?= $modal_padding; ?> <?= $modal_padding; ?>;
        }

        #dlg-footer button {
            background-color: black;
            color: white;
            padding: 5px;
            border: 0px;
        }

    </style>

</head>
<body>

<!-- dialog box -->
<div id="white-background"></div>
<div id="dlgbox">
    <div id="dlg-header">You are logged out.</div>
    <div id="dlg-body">Kindly log in to continue.</div>
    <div id="dlg-footer">
        <button onclick="dlgLogin()">Log in</button>
    </div>
</div>

    <div class="login-box">
        <div class="login-box-body">
            <div class="login-logo"><img src="images/logo1.png"></div><!-- /.login-logo -->
            <div id="notify"></div><span id="login-panel-wrapper"></span>
            <form onsubmit="return onForgotPassword();">
                <span id="login-panel-wrapper"></span>
                <div class="form-group has-feedback">
                    <span id="login-panel-wrapper"><input class="form-control" name="email" id="email" placeholder="User Name (name@company)" type="text" required> <span class="glyphicon glyphicon-envelope form-control-feedback"></span></span>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <button class="reset-btn" id="reset-btn" type="submit" value="Login">Send</button>
                    </div><!-- /.col -->
                    <br><br>
                </div>
            </form>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <p>Want to join the community? <a href="https://guestkarma.com/pages/enquire">Enquire</a> </p>

<script>
    function onForgotPassword() {
        var button = document.getElementById("reset-btn");
        var email = document.getElementById("email").value;

        var italic = document.createElement("i");
        italic.setAttribute("class", "fa fa-spinner fa-spin");
        italic.style.marginLeft = "5px";
        button.appendChild(italic);

        $.ajax({
            url: "http.php",
            method: "POST",
            data: {forgotPassword: 1, email: email},
            success: function (response) {
                console.log(response);
                button.removeChild(italic);

                var data = JSON.parse(response);
                if (data.error == "") {
                    showDialog("Reset password", "A link to reset your password has been sent on your email address", "<button onclick='window.location.reload();'>OK</button>");
                } else {
                    showDialog("Error", data.error, "<button onclick='closeDialog();'>Try again</button>");
                }
            }
        });
        return false;
    }

    function showDialog(header, body, footer) {
        $("#white-background").show();
        $("#dlgbox").show();

        var winWidth = window.innerWidth;
        var winHeight = window.innerHeight;

        $("#dlgbox").css({
            left: (winWidth / 2) - (480 / 2) + "px",
            top: "150px"
        });

        $("#dlg-header").html(header);
        $("#dlg-body").html(body);
        $("#dlg-footer").html(footer);
    }

    function closeDialog() {
        $("#white-background").hide();
        $("#dlgbox").hide();
    }
</script>

</body>
</html>
