<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

if (isset($_POST['add_client'])) {
    $data = json_decode($_POST['data']);
    $hotelName = trim($data->hotelName);
    $email = trim($data->email);

    $response = array();
    $response["error"] = "";

    $stmt = $mysqli->prepare("SELECT * FROM clients WHERE email = ? OR username = ?");
    $stmt->bind_param("ss", $email, $hotelName);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $response["error"] = "Client with this name or email already exists";
    } else {
        $token = md5($hotelName . "-" . time());
        $html = "Your account has been successfully created. Please follow the link to setup your password.<br />";
        // TODO Change URL here

        $html .= "<a href='http://localhost/create-password.php?createPassword=1&email=$email&token=$token'>Create password</a>";
        $response["error"] = send_mail($email, "Account created", $html);

        if (empty($response["error"])) {
            $query = "INSERT INTO clients (email, username, token) VALUES (?, ?, ?)";

            $stmt = $mysqli->prepare($query);
            $stmt->bind_param("sss", $email, $hotelName, $token);
            $stmt->execute();
        }
    }

    echo json_encode($response);
}
?>
