<?php

include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
require 'PHPMailer/PHPMailerAutoload.php';

sec_session_start(); // Our custom secure way of starting a PHP session.

if (isset($_POST["search"])) {
    $response = array();
    $response["complaints"] = array();
    $response["msg"] = "";
    $response["error"] = "";

    //Sanitize data
    $firstName = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
    $firstName = trim($firstName);
    $lastName = filter_input(INPUT_POST, trim('lastName'), FILTER_SANITIZE_STRING);
    $lastName = trim($lastName);
    $dob = filter_input(INPUT_POST, trim('dob'), FILTER_SANITIZE_STRING);
    $dob = trim($dob);

    $error = "";
    if (empty($firstName)) {
        $error .= "Please enter first name<br>";
    }
    if (empty($lastName)) {
        $error .= "Please enter last name<br>";
    }
    if (empty($dob)) {
        $error .= "Please enter date of birth<br>";
    }

    if (empty($error)) {
        $stmt = $mysqli->prepare("SELECT * FROM person INNER JOIN complaints ON person.person_id = complaints.person_id WHERE person.first_name = ? AND person.surname = ? AND person.dob = ?");
        $stmt->bind_param('sss', $firstName, $lastName, $dob);
        $stmt->execute();    // Execute the prepared query.
        $result = $stmt->get_result();
        $client_id = $_SESSION['client_id'];

        while ($complaint = $result->fetch_assoc()) {
            $response["complaints"][] = $complaint;
        }
        $stmt = $mysqli->prepare("UPDATE client_statistics SET customers_checked = customers_checked + 1 WHERE client_id = ?");
        $stmt->bind_param('i', $client_id);
        $stmt->execute();

        if ($result->num_rows > 0) {
            $stmt = $mysqli->prepare("UPDATE client_statistics SET bad_guests_detected = bad_guests_detected + 1 WHERE client_id = ?");
            $stmt->bind_param('i', $client_id);
            $stmt->execute();
        }
    } else {
        $response["error"] = $error;
    }
    echo json_encode($response);
} else if (isset($_POST["add_complaint"])) {
    $response = array();
    $response["msg"] = "";
    $response["error"] = "";

    $data = json_decode($_POST["data"]);
    //Set and trim variables
    $firstName = !empty(trim($data->firstName)) ? trim($data->firstName) : "NULL";
    $lastName = !empty(trim($data->lastName)) ? trim($data->lastName) : "NULL";
    $gender = !empty(trim($data->gender)) ? trim($data->gender) : "NULL";
    $dob = !empty(trim($data->dob)) ? trim($data->dob) : "NULL";
    $phoneNumber1 = !empty(trim($data->phoneNumber1)) ? trim($data->phoneNumber1) : "NULL";
    $phoneNumber2 = !empty(trim($data->phoneNumber2)) ? trim($data->phoneNumber2) : "NULL";
    $country = !empty(trim($data->country)) ? trim($data->country) : "NULL";
    $state = !empty(trim($data->state)) ? trim($data->state) : "NULL";
    $city = !empty(trim($data->city)) ? trim($data->city) : "NULL";
    $suburb = !empty(trim($data->suburb)) ? trim($data->suburb) : "NULL";
    $postCode = !empty(trim($data->postCode)) ? trim($data->postCode) : "NULL";
    $complaintType = !empty(trim($data->complaintType)) ? trim($data->complaintType) : "NULL";
    $complaintDetails = !empty(trim($data->complaintDetails)) ? trim($data->complaintDetails) : "NULL";
    $email = !empty(trim($data->email)) ? trim($data->email) : "NULL";
    $cost = !empty(trim($data->cost)) ? trim($data->cost) : "NULL";

    $error = "";
    if (empty($firstName)) {
        $error .= "Please enter first name<br>";
    }
    if (empty($lastName)) {
        $error .= "Please enter last name<br>";
    }
    if (empty($gender)) {
        $error .= "Please select gender<br>";
    }
    if (empty($dob)) {
        $error .= "Please enter date of birth<br>";
    }
    if (empty($phoneNumber1)) {
        $error .= "Please enter phone number 1<br>";
    }
    if (empty($phoneNumber2)) {
        $error .= "Please enter phone number 2<br>";
    }
    if (empty($country)) {
        $error .= "Please enter country<br>";
    }
    if (empty($state)) {
        $error .= "Please select state<br>";
    }
    if (empty($suburb)) {
        $error .= "Please enter suburb<br>";
    }
    if (empty($postCode)) {
        $error .= "Please enter post code<br>";
    }
    if (empty($complaintType)) {
        $error .= "Please select complaint type<br>";
    }
    if (empty($complaintDetails)) {
        $error .= "Please enter complaint details<br>";
    } else if (strlen($complaintDetails) < 10) {
        $error .= "Complaint length should be at least of 10 characters";
    }
    if (empty($email)) {
        $error .= "Please enter email<br>";
    }
    if (empty($cost)) {
        $error .= "Please enter cost<br>";
    }

    if (empty($error)) {
        $statement = $mysqli->prepare("SELECT * FROM `person` WHERE `first_name`= ? AND `surname`= ? AND `dob`= ?");
        $statement->bind_param("sss", $firstName, $lastName, $dob);
        $statement->execute();

        $result = $statement->get_result();

        if ($result->num_rows > 0) {
            $person = $result->fetch_object();
            $person_id = $person->person_id;

            $response["complain"] = insert_complain($person_id);
        } else {
            $statement = $mysqli->prepare("INSERT INTO `person`(`Suburb`, `state`, `Post_Code`, `country`, `phone_number_1`, `phone_number_2`, `email`, `surname`, `first_name`, `city`, `dob`, client_id, cost) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $statement->bind_param("sssssssssssss", $suburb, $state, $postCode, $country, $phoneNumber1, $phoneNumber2, $email, $lastName, $firstName, $city, $dob, $_SESSION['client_id'], $cost);

            $response["person"] = "INSERT INTO `person`(`Suburb`, `state`, `Post_Code`, `country`, `phone_number_1`, `phone_number_2`, `email`, `surname`, `first_name`, `city`, `dob`, client_id, cost) VALUES ('$suburb', '$state', '$postCode', '$country', '$phoneNumber1', '$phoneNumber2', '$email', '$lastName', '$firstName', '$city', '$dob', '" . $_SESSION['client_id'] . "', '$cost')";

            $statement->execute();
            $person_id = $mysqli->insert_id;
            $response["complain"] = insert_complain($person_id);

            $client_id = $_SESSION['client_id'];
            $stmt = $mysqli->prepare("UPDATE client_statistics SET reports_made = reports_made + 1 WHERE client_id = ?");
            $stmt->bind_param('i', $client_id);
            $stmt->execute();
        }
    } else {
        $response["error"] = $error;
    }

    echo json_encode($response);
} else if (isset($_POST["createPassword"])) {
    $email = $_POST["email"];
    $token = $_POST["token"];
    $password = $_POST["password"];

    $response = array();
    $response["error"] = "";
    if (empty($email) || empty($token) || empty($password)) {
        $response["error"] = "Please fill all fields";
    } else {
        $hashed_password = password_hash($password, PASSWORD_BCRYPT);
        $sql = "UPDATE clients SET password = ? WHERE email = ? AND token = ?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("sss", $hashed_password, $email, $token);
        $stmt->execute();
    }

    echo json_encode($response);
} else if (isset($_POST["deleteClient"])) {
    $clientId = $_POST["clientId"];

    $sql = "SELECT * FROM clients WHERE client_id = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $clientId);
    $stmt->execute();
    $result = $stmt->get_result();
    $client = $result->fetch_object();

    $html = "Hi <b>$client->username,</b><br>It is to inform you that your account has been deleted from administration.";
    send_mail($client->email, "Account deleted", $html);

    //Delete client
    $sql = "DELETE FROM clients WHERE client_id = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $clientId);
    $stmt->execute();

    //Delete client stats
    $sql = "DELETE FROM client_statistics WHERE client_id = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $clientId);
    $stmt->execute();

} else if (isset($_POST["getstates"])) {
    $countryCode = $_POST["countryCode"];
    $data = array();

    $sql = "SELECT * FROM `provinces` WHERE `country_id` = ? ORDER BY name ASC";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $countryCode);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        for ($a = 0; $a < $result->num_rows; $a++) {
            $state = $result->fetch_object();
            $data[]["name"] = htmlentities(utf8_encode($state->name), 0, "UTF-8");
        }
    }

    echo json_encode($data);
} else if (isset($_POST["lockUnlockClient"])) {
    $response = array();

    $clientId = $_POST["clientId"];
    $state = $_POST["state"];

    $sql = "SELECT * FROM clients WHERE client_id = ?";
    $response["sql1"] = $sql;

    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("s", $clientId);
    $stmt->execute();
    $result = $stmt->get_result();
    $client = $result->fetch_object();

    // TODO un-comment here
    if ($state == 1) {
        send_account_locked_email($client->email);
    } else {
        $html = "Hi <b>$client->username,</b><br>It is to inform you that your account has been unlocked from administration.";
        send_mail($client->email, "Account unlocked", $html);
    }

    //Delete client
    $sql = "UPDATE clients SET locked = ? WHERE client_id = ?";
    $response["sql2"] = array();
    $response["sql2"]["sql"] = $sql;
    $response["sql2"]["clientId"] = $clientId;
    $response["sql2"]["state"] = $state;

    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("ss", $state, $clientId);
    $stmt->execute();

    echo json_encode($response);
} else if (isset($_POST["add_client"])) {
    $data = json_decode($_POST['data']);
    $hotelName = trim($data->hotelName);
    $email = trim($data->email);

    $response = array();
    $response["error"] = "";

    if (empty($hotelName) || empty($email)) {
        $response["error"] = "Please fill all fields";
    } else {
        $stmt = $mysqli->prepare("SELECT * FROM clients WHERE email = ? OR username = ?");
        $stmt->bind_param("ss", $email, $hotelName);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $response["error"] = "Client with this name or email already exists";
        } else {
            $token = md5($hotelName . "-" . time());
            $html = "Welcome to <b>Guest Karma</b>,<br>
                 Your account has been successfully created. <br>Now, please follow the link to setup your password.
                 This link will expire in 24 hours.<br>";
            // TODO Change URL here
            $html .= "<a href='http://localhost/create-password.php?createPassword=1&email=$email&token=$token'>Create password</a>";
            $response["error"] = send_mail($email, "Account created", $html);

            if (empty($response["error"])) {
                $mailed_at = time();
                $query = "INSERT INTO clients (email, username, token, mailed_at) VALUES (?, ?, ?, ?)";

                $stmt = $mysqli->prepare($query);
                $stmt->bind_param("ssss", $email, $hotelName, $token, $mailed_at);
                $stmt->execute();

                $client_id = $mysqli->insert_id;
                $query = "INSERT INTO client_statistics (client_id) VALUES (?)";
                $stmt = $mysqli->prepare($query);
                $stmt->bind_param("i", $client_id);
                $stmt->execute();
            }
        }
    }

    echo json_encode($response);
} else if (isset($_POST["forgotPassword"])) {
    $email = $_POST["email"];
    $response = array();
    $response["error"] = "";

    if (empty($email)) {
        $response["error"] = "Please fill all fields";
    } else {
        $stmt = $mysqli->prepare("SELECT * FROM clients WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $member = $result->fetch_object();
            $client_id = $member->client_id;
            $time = time();
            $token = md5($time);

            $stmt = $mysqli->prepare("UPDATE clients SET token = ?, mailed_at = ? WHERE client_id = ?");
            $stmt->bind_param("sss", $token, $time, $client_id);
            $stmt->execute();

            // TODO change here
            $url = "http://localhost/reset-password.php?email=$email&token=$token";
            $html = "Hi <b>$member->username</b>,<br><br>Please click on the following link to reset your password <br><a href='$url'>Reset Password</a>";
            send_mail($email, "Reset password", $html);
        } else {
            $response["error"] = "Account with this email does not exists";
        }
    }

    echo json_encode($response);
}

function insert_complain($person_id)
{
    global $mysqli, $complaintType, $complaintDetails;
    $sql = "INSERT INTO `complaints`(`complaint_type`, `complaint_description`, `time_added`, `person_id`, client_id) VALUES ('$complaintType', '$complaintDetails', NOW(), '$person_id', '" . $_SESSION['client_id'] . "')";
    $statement = $mysqli->prepare("INSERT INTO `complaints`(`complaint_type`, `complaint_description`, `time_added`, `person_id`, client_id) VALUES (?, ?, NOW(), ?, ?)");
    $statement->bind_param("ssss", $complaintType, $complaintDetails, $person_id, $_SESSION['client_id']);
    $statement->execute();
    return $sql;
}
