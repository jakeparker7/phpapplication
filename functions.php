<?php
include_once 'psl-config.php';
function sec_session_start()
{
    $session_name = 'sec_session_id';   // Set a custom session name 
    $secure = SECURE;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session 
    session_regenerate_id();    // regenerated the session, delete the old one. 
}

function delete_login_attempts($client_id, $mysqli)
{
    $statement = $mysqli->prepare("DELETE FROM login_attempts WHERE client_id = ?");
    $statement->bind_param("s", $client_id);
    $statement->execute();
}

function login($email, $password, $mysqli)
{
    // Using prepared statements means that SQL injection is not possible. 
    if ($stmt = $mysqli->prepare("SELECT client_id, username, password, admin 
        FROM clients
       WHERE email = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $email);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();

        // get variables from result.
        $stmt->bind_result($client_id, $username, $db_password, $admin);
        $stmt->fetch();

        if ($stmt->num_rows == 1) {
            // Check if account has been locked
            $statement = $mysqli->prepare("SELECT locked FROM clients WHERE client_id = ?");
            $statement->bind_param("s", $client_id);
            $statement->execute();
            $result = $statement->get_result();
            $result = $result->fetch_object();
            if ($result->locked == 1) {
                $_SESSION[KEY_ERROR] = LOCKED_ACCOUNT_MESSAGE;
                return false;
            }

            // If the user exists we check if the account is locked
            // from too many login attempts
            if (checkbrute($client_id, $mysqli) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
                $statement = $mysqli->prepare("UPDATE clients SET locked = 1 WHERE client_id = ?");
                $statement->bind_param("s", $client_id);
                $statement->execute();

                delete_login_attempts($client_id, $mysqli);
                send_account_locked_email($email);

                echo "checkbrute <br>";
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted. We are using
                // the password_verify function to avoid timing attacks.
                echo "___Password: " . $password;
                echo "<br>DB Password: " . $db_password . "<br>";
                echo "Hash: " . password_hash($password, PASSWORD_BCRYPT);
                if (password_verify($password, $db_password) /*$password == $db_password*/) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $client_id = preg_replace("/[^0-9]+/", "", $client_id);
                    $_SESSION['client_id'] = $client_id;
                    $_SESSION['admin'] = $admin;
                    // XSS protection as we might print this value
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/",
                        "",
                        $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512',
                        $db_password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(client_id, time)
                                    VALUES ('$client_id', '$now')");

//                    $_SESSION[LOGIN_ATTEMPTS] = ((int)$_SESSION[LOGIN_ATTEMPTS] + 1);
                    //echo "<script>alert('Invalid Login Details');</script>";
                    return false;
                }
            }
        } else {
            // No user exists.
            echo "<script>document.getElementById('password').style.border = 'red';</script>";
            return false;
        }
    }
}

function send_account_locked_email($email)
{
    send_mail($email, "Account locked", LOCKED_ACCOUNT_MESSAGE);
}

function checkbrute($client_id, $mysqli)
{
//    return false;

    // Get timestamp of current time 
    $now = time();

    // All login attempts are counted from the past 2 hours. 
    $valid_attempts = $now - (2 * 60 * 60);

    if ($stmt = $mysqli->prepare("SELECT time 
                             FROM login_attempts 
                             WHERE client_id = ? 
                            AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $client_id);

        // Execute the prepared query. 
        $stmt->execute();
        $stmt->store_result();

        $_SESSION[LOGIN_ATTEMPTS] = $stmt->num_rows + 1;

        // If there have been more than 5 failed logins
        // TODO change here
        if ($_SESSION[LOGIN_ATTEMPTS] >= MAX_LOGIN_ATTEMPTS) {
            return true;
        } else {
            return false;
        }
    }
}


function login_check($mysqli)
{
    // Check if all session variables are set 
    if (isset($_SESSION['client_id'],
        $_SESSION['username'],
        $_SESSION['login_string'])) {

        $client_id = $_SESSION['client_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];

        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        if ($stmt = $mysqli->prepare("SELECT password 
                                      FROM clients 
                                      WHERE client_id = ? LIMIT 1")) {
            // Bind "$client_id" to parameter. 
            $stmt->bind_param('i', $client_id);
            $stmt->execute();   // Execute the prepared query.
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                // If the user exists get variables from result.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
                if (hash_equals($login_check, $login_string)) {
                    // Logged In!!!! 
                    return true;
                } else {
                    // Not logged in 
                    return false;
                }
            } else {
                // Not logged in 
                return false;
            }
        } else {
            // Not logged in 
            return false;
        }
    } else {
        // Not logged in 
        return false;
    }
}

function esc_url($url)
{

    if ('' == $url) {
        return $url;
    }

    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);

    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string)$url;

    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }

    $url = str_replace(';//', '://', $url);

    $url = htmlentities($url);

    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);

    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}

function send_mail($to, $subject, $body)
{
    $mail = new PHPMailer;
//    $mail->SMTPDebug = 3;                               // Enable verbose debug output

    // TODO change here

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'admin@guestkarma.com';                 // SMTP username
    $mail->Password = 'n5C9>VR2!!';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('admin@guestkarma.com', 'Admin - Guest Karma');
    $mail->addAddress($to);     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $body;

    if ($mail->send()) {
    return '';
    } else {
        return 'Mailer Error: ' . $mail->ErrorInfo;
    }
}

function send_mail_attachment($to, $subject, $body, $attachment)
{
	require_once 'PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
//    $mail->SMTPDebug = 3;                               // Enable verbose debug output

    // TODO change here

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'admin@guestkarma.com';                 // SMTP username
    $mail->Password = 'n5C9>VR2!!';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('admin@guestkarma.com', 'Admin - Guest Karma');
    $mail->addAddress($to);     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->addAttachment($attachment);

    $mail->Subject = $subject;
    $mail->Body = $body;

    if ($mail->send()) {
        return '';
    } else {
        return 'Mailer Error: ' . $mail->ErrorInfo;
    }
}
?>
