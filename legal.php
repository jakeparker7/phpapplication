<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
 sec_session_start();
require_once 'includes/header.php';

if (!isset($_SESSION['client_id'])) {
    header("Location: index.php");
}

?>
 <!DOCTYPE html>
<?php if (login_check($mysqli) == true) : ?>
 <div class="clearfix"></div>
   <div class="c-box1"> 
    <div class="col-lg-12">
      <div class="col-lg-offset-4 col-lg-4">
        <p class="title1">Legal</p><br>
        <hr>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="container">
    <div class="c-cont1">
      <p>
        The following Legal information is full and complete, if you have any issues or complaints e-mail us at info@guestkarma.com or call us on 0411 168 070. 
        <br><br>
      </p>
    </div>

    <div class="c-box2">
      <p class="title2">Legal Documents</p>
      <hr class="c-brdr1">
      <div class="form c-form">
       <br>
        <a href="./pdf/Member Agreement.pdf">Member Agreement</a>
        <br>
        <a href="./pdf/Letter to bad guest.doc">Letter to bad guest</a>
        <br>
        <a href="./pdf/Privacy statement.pdf">Privacy statement</a>
        <br>
        <a href= "./pdfT&C.pdf">Term's & Conditions</a>
      </div>
    </div>

  </div>

  <?php require_once 'includes/footer.php';?>
  </body>
</html>
  <?php else :
      header('Location: ../index.php');
  endif; ?>
