<?php

//error_reporting(0);

define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);

require_once 'includes/db_connect.php';
require_once 'includes/functions.php';
$msg = "";

$target_dir = "";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if file already exists
if (file_exists($target_file)) {
    $msg .= "Sorry, file already exists<br>";
    $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 5 * MB) {
    $msg .= 'Sorry, your file is too large<br>';
    $uploadOk = 0;
}

// Allow certain file formats
if ($fileType != "csv" && $fileType != "zip") {
    $msg .= 'Sorry, only CSV and ZIP files can be uploaded<br>';
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
//    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        // TODO and change here too
        send_mail_attachment("admin@guestkarma.com", "Bad Guests list", "Please find the list of bad guests in the attachment", $target_file);
		$msg .= 'The file ' . basename($_FILES["fileToUpload"]["name"]) . ' has been uploaded';
    } else {
        $msg .= 'Sorry, there was an error uploading your file<br>';
    }
}

unlink($target_file);
echo "Message: " . $msg;
//header("Location: " . $_SERVER['HTTP_REFERER']);
?>
