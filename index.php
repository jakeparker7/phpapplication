<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
require 'PHPMailer/PHPMailerAutoload.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
    session_destroy();
    header('Location: home.php');
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="/images/tab_logo.ico"/>
    <link href="css/bootstrap.min.css?jNiMS7GqGhSVkYvtqohaMSAFspb3KXnG38dZrIsb" rel="stylesheet" type="text/css">
    <link href="css/AdminLTE.min.css?jNiMS7GqGhSVkYvtqohaMSAFspb3KXnG38dZrIsb" rel="stylesheet" type="text/css">
    <title>Guest Karma</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Lato:100,300,400,700,900|Montserrat:100,200,300,400,500,600,700'
          rel='stylesheet' type='text/css'>
    <script src="js/sha512.js" type="text/JavaScript">
    </script>
    <script src="js/forms.js" type="text/JavaScript">
    </script>
</head>
<body>

<?php
if (isset($_GET['error'])) {
    echo "<script>alert('Invalid Login Details');</script>";
}

if(isset($_GET['dc'])) {
    echo "<script>alert('An Internal Error has occured. If this persists contact Guest Karma on support@guestkarma.com');</script>";
}
if (isset($_SESSION[KEY_ERROR])) {
    echo "<p style='color: red;
    position: relative;
    left: 70px;
    top: 70px;'>" . LOCKED_ACCOUNT_MESSAGE . "</p>";
    unset($_SESSION[KEY_ERROR]);
}
?>

<div class="login-box">
    <div class="login-box-body">
        <div class="login-logo"><img src="images/logo1.png"></div><!-- /.login-logo -->
        <div id="notify"></div>
        <span id="login-panel-wrapper"></span>
        <form action="includes/process_login.php" method="post">
            <span id="login-panel-wrapper"></span>
            <div class="form-group has-feedback">
                <span id="login-panel-wrapper"><input class="form-control" name="email"
                                                      placeholder="User Name (name@company)" type="text"> <span
                            class="glyphicon glyphicon-envelope form-control-feedback"></span></span>
            </div>
            <div class="form-group has-feedback">
                <input class="form-control" id="password" name="password" placeholder="Password" type="password"> <span
                        class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <a href="forgot_password.php">Forgot your password?</a>
            <br><br>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <!-- <label>
                    <input type="checkbox"> Remember Me
                </label> -->
                    </div>
                </div><!-- /.col -->
                <br><br>
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <?php
                    if (isset($_GET["attemps"])) {
                        echo "<p>You have " . $_GET["attemps"] . " attempt(s) left</p>";
                    }
                    ?>

                </div><!-- /.col -->

                <div class="col-xs-4">
                    <button class="login-btn" onclick="formhash(this.form, this.form.password);" type="submit"
                            value="Login">Login
                    </button>
                </div><!-- /.col -->

            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<p>Want to join the community? <a href="https://guestkarma.com/pages/enquire">Enquire</a></p>

</body>
</html>
