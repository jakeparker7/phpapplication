<div class="c-box2">
    <p class="title2">Add A Client</p>
    <hr class="c-brdr1">
    <div class="form c-form">
        <form id="addClientForm" method="post" name="reportForm" onsubmit="return confirmAddClient();">
            <div class="col-lg-12 p-0">
                <div class="col-lg-4 p-0">
                    <div class="p-r-15">
                        <label for="">Hotel Name</label> <input class="form-control cstm-form-control" id="hotelName"
                                                                name="hotelName" type="text" required>
                    </div>
                </div>
                <div class="col-lg-4 p-0">
                    <div class="p-r-15">
                        <label for="">email</label> <input class="form-control cstm-form-control" id="email_add"
                                                           name="email_add" type="email" required>
                    </div>
                </div>
                <input type="submit" value="Add" name="submit" class="cstm-btn"
                       style="margin-top: 29px; padding: 10px 35px;">
            </div>
        </form>
    </div>
</div>

<?php
$sql = "SELECT * FROM clients WHERE admin IS NULL ORDER BY client_id DESC";
$stmt = $mysqli->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$num_of_rows = $result->num_rows;
?>

<div class="c-box2">
    <p class="title2">Clients</p>
    <hr class="c-brdr1">
    <div class="form c-form">
        <br>
        <table class="table">
            <tr>
                <th>Name</th>
                <th>email</th>
                <th style="text-align: center;" colspan="2">Action</th>
            </tr>

            <?php
            if ($num_of_rows > 0) {
                while ($row = $result->fetch_object()) {
                    ?>
                    <tr>
                        <td><?= $row->username; ?></td>
                        <td><?= $row->email; ?></td>
                        <td>
                            <button class="cstm-btn" data-client-id="<?= $row->client_id; ?>"
                                    onclick="confirmClientDelete(this);" style="padding: 5px 20px;">
                                Remove
                            </button>
                        </td>
                        <td>
                            <button class="cstm-btn" data-client-id="<?= $row->client_id; ?>"
                                    data-lock-state="<?= $row->locked; ?>"
                                    style="padding: 6px; width: 102px; background: <?= $row->locked == 1 ? 'red' : 'green'; ?>"
                                    onclick="confirmLockUnlock(this);">
                                <?= $row->locked == 1 ? 'Unlock' : 'Lock'; ?>
                            </button>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                echo '<tr><td colspan="4" align="center" style="border-bottom: 1px solid #ddd;">No Result found</td></tr>';
            }
            ?>
        </table>
    </div>
</div>

<script>
    function confirmClientDelete(deleteBtn) {
        clientId = deleteBtn.getAttribute("data-client-id");
        var footer = "<button onclick='closeDialog();'>No</button> <button onclick='deleteClient(clientId, this);'>Yes</button>";

        showDialog("Confirm delete", "Are you sure you want to delete this client ?", footer);
    }

    function deleteClient(clientId, deleteBtn) {
        var italic = document.createElement("i");
        italic.setAttribute("class", "fa fa-spinner fa-spin");
        italic.style.marginLeft = "5px";
        deleteBtn.appendChild(italic);

        $.ajax({
            url: "http.php",
            method: "POST",
            data: {deleteClient: 1, clientId: clientId},
            success: function (response) {
                console.log(response);
                deleteBtn.removeChild(italic);
                window.location.reload();
            }
        });
    }

    function confirmLockUnlock(btn) {
        clientId = btn.getAttribute("data-client-id");
        isLock = btn.getAttribute("data-lock-state");

        var footer = "<button onclick='closeDialog();'>No</button> <button onclick='lockUnlockClient(clientId, isLock, this);'>Yes</button>";

        showDialog("Confirm " + (isLock == 1 ? "Unlock" : "Lock"), "Are you sure you want to " + (isLock == 1 ? 'unlock' : 'lock') + " this client ?", footer);
    }

    function lockUnlockClient(clientId, lock, btn) {
        var italic = document.createElement("i");
        italic.setAttribute("class", "fa fa-spinner fa-spin");
        italic.style.marginLeft = "5px";
        btn.appendChild(italic);

        $.ajax({
            url: "http.php",
            method: "POST",
            data: {lockUnlockClient: 1, clientId: clientId, state: (lock == 1 ? 0 : 1)},
            success: function (response) {
                btn.removeChild(italic);
                window.location.reload();
            }
        });
    }
</script>
