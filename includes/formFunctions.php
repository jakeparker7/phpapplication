<?php
function makeHttpRequest(method, to, callBack, data) {
	var ajax = new XMLHttpRequest();
	method = method.toUpperCase();
	ajax.open(method, to, true);
	if(method == "POST") {
		ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajax.send(data);
	} else {
		ajax.send();
	}
	ajax.onreadystatechange = function() {
		if (this.statusText === "OK") {
			callBack(this.responseText);
		}
	}
}

function toggleReportCustomerModal() {
	$("#reportSubmitModal").modal("toggle");
	//$("#modal-table").toggle();
}