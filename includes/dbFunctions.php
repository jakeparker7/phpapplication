<script>
    function submitAddClientemail() {
        $("#add-complaint-loading").show();
        var data = {
            hotelName: hotelName,
            email: email
        };
        $.ajax({
            url: "http.php",
            method: "POST",
            data: {data: JSON.stringify(data), add_client: 1},
            success: function (response) {
                console.log(response);
                var data = JSON.parse(response);

                if (data.error !== "") {
                    showDialog("Error", data.error, "<button onclick='closeDialog();'>Try again</button>");
                } else {
                    showDialog("Success", "You have added the client", "<button onclick='closeDialog();'>OK</button>");
                    document.getElementById("addClientForm").reset();
                }

                $("#add-complaint-loading").hide();
            }
        });
    }

    function submitReportToDb() {
        $("#add-complaint-loading").show();
        var data = {
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            dob: dob,
            phoneNumber1: phoneNumber1,
            phoneNumber2: phoneNumber2,
            country: country,
            state: state,
            city: city,
            suburb: suburb,
            postCode: postCode,
            complaintType: complaintType,
            complaintDetails: complaintDetails,
            email: email,
            cost: cost
        };

        $.ajax({
            url: "http.php",
            method: "POST",
            data: {data: JSON.stringify(data), add_complaint: 1},
            success: function (response) {
                console.log(response);
                var data = JSON.parse(response);

                if (data.error == "") {
                    showDialog("Success", "Your report has been submitted", "<button onclick='closeDialog();'>OK</button>");
                    document.getElementById("reportForm").reset();
                } else {
                    showDialog("Error", data.error, "<button onclick='closeDialog();'>Try Again</button>");
                }

                $("#add-complaint-loading").hide();
            }
        });
    }
</script>
