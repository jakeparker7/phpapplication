<?php
include_once 'db_connect.php';
include_once 'functions.php';
require '../PHPMailer/PHPMailerAutoload.php';

sec_session_start(); // Our custom secure way of starting a PHP session.

if (isset($_POST['email'], $_POST['p'])) {
    $email = $_POST['email'];
    $password = $_POST['p']; // The hashed password.
    $_SESSION[LOGIN_ATTEMPTS] = 0;

    if (login($email, $password, $mysqli) == true) {
        // Login success
        //$admin = $_SESSION['admin']; 
        //if($admin == 1) {
        //    echo 'adminnn';
        //} else {
        //   echo htmlentities($_SESSION['admin']);

        delete_login_attempts($_SESSION['client_id'], $mysqli);
        header('Location: ../home.php');
        //}
    } else {
        // Login failed

        // TODO change here
        if ($_SESSION[LOGIN_ATTEMPTS] >= THRESHOLD_LOGIN_ATTEMPTS) {
            $attempts = (MAX_LOGIN_ATTEMPTS - $_SESSION[LOGIN_ATTEMPTS]);
            if ($attempts > 0) {
                header('Location: ../index.php?error=1&attemps=' . $attempts);
            } else {
                $statement = $mysqli->prepare("UPDATE clients SET locked = 1 WHERE client_id = ?");
                $statement->bind_param("s", $client_id);
                $statement->execute();

                delete_login_attempts($client_id, $mysqli);
                send_account_locked_email($email);

                $_SESSION[KEY_ERROR] = LOCKED_ACCOUNT_MESSAGE;
                header('Location: ../index.php?error=1');
            }
        } else {
            header('Location: ../index.php?error=1');
        }

    }
} else {
    // The correct POST variables were not sent to this page. 
    echo 'Invalid Request';
}
?>
