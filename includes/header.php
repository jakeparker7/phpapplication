<?php

$modal_padding = "5px";
?>

<!DOCTYPE html>
<html>
<head>
    <title>Guest Karma</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="icon" href="/images/tab_logo.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Lato:100,300,400,700,900|Montserrat:100,200,300,400,500,600,700' rel='stylesheet' type='text/css'/>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
<style>

#white-background {
	display: none;
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0px;
	left: 0px;
	background-color: #fefefe;
	opacity: 0.7;
	z-index: 9999;
}

#dlgbox {
	display: none;
	position: fixed;
	width: 480px;
	z-index: 9999;
	border-radius: 5px;
	background-color: #7c7d7e;
}

#dlg-header {
	background-color: black;
	color: white;
	font-size: 20px;
	padding: 10px;
	margin: <?= $modal_padding; ?> <?= $modal_padding; ?> 0px <?= $modal_padding; ?>;
}

#dlg-body {
	background-color: white;
	color: black;
	font-size: 14px;
	padding: 10px;
	margin: 0px <?= $modal_padding; ?> 0px <?= $modal_padding; ?>;
	overflow-y: scroll;
	max-height: 300px;
}

#dlg-footer {
	background-color: #f2f2f2;
	text-align: right;
	padding: 10px;
	margin: 0px <?= $modal_padding; ?> <?= $modal_padding; ?> <?= $modal_padding; ?>;
}

#dlg-footer button {
	background-color: black;
	color: white;
	padding: 5px;
	border: 0px;
}

</style>
</head>
<body>

<!-- dialog box -->
<div id="white-background"></div>
<div id="dlgbox">
    <div id="dlg-header">You are logged out.</div>
    <div id="dlg-body">Kindly log in to continue.</div>
    <div id="dlg-footer">
        <button onclick="dlgLogin()">Log in</button>
    </div>
</div>

<!-- header starts here -->
<div class="cstm-menu" id="navbar-main">
    <div class="container">
        <div class="col-lg-12 col-xs-12">
            <div class="col-lg-3 col-xs-8">
                <img src="images/logo1.png" class="img-responsive">
            </div>
            <div class="col-lg-9 col-xs-4">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse navHeaderCollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="home.php">Home</a></li>
                        <li><a href="blackList.php">Black List</a></li>
                        <li><a href="legal.php">Legal</a></li>
                        <li><a href="includes/logout.php">Log Out</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Header end -->
