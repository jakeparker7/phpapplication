<script>

    function validateReport() {
        var errorFirstName = document.getElementById("errorFirstName");
        var errorLastName = document.getElementById("errorLastName");
        var errorDate = document.getElementById("errorDate");
        var errorEmail = document.getElementById("errorEmail");
        var errorPhone1 = document.getElementById("errorPhone1");
        var errorPhone2 = document.getElementById("errorPhone2");
        var errorCity = document.getElementById("errorCity");
        var errorSuburb = document.getElementById("errorSuburb");
        var errorPostCode = document.getElementById("errorPostCode");
        var errorCost = document.getElementById("errorCost");
        var errorComplaintDetails = document.getElementById("errorComplaintDetails");

        //TODO:  SHOULD CALL A FUNCTION HERE TO RESET BORDERS
        var borderColor = "#ccc";
        document.getElementById('first_name').style.borderColor = borderColor;
        document.getElementById('last_name').style.borderColor = borderColor;
        document.getElementById('email').style.borderColor = borderColor;
        document.getElementById('gender').style.borderColor = borderColor;
        document.getElementById('dob').style.borderColor = borderColor;
        document.getElementById('phone_number_1').style.borderColor = borderColor;
        document.getElementById('phone_number_2').style.borderColor = borderColor;
        document.getElementById('country').style.borderColor = borderColor;
        document.getElementById('state').style.borderColor = borderColor;
        document.getElementById('city').style.borderColor = borderColor;
        document.getElementById('suburb').style.borderColor = borderColor;
        document.getElementById('post_code').style.borderColor = borderColor;
        document.getElementById('complaint_type').style.borderColor = borderColor;
        document.getElementById('complaint_details').style.borderColor = borderColor;
        var errorNodes = document.getElementsByClassName("error");
        for (var a = 0; a < errorNodes.length; a++) {
            errorNodes[a].style.display = "none";
        }

        //TODO: SHOULD ALSO LOOK AT CALLING A FUNCTION THAT HIGHLIGHTS ALL REQUIRED
        firstName = document.getElementById("first_name").value;
        lastName = document.getElementById("last_name").value;
        email = document.getElementById("email").value;
        genderSelector = document.getElementById("gender");
        //Getting selected Gender value
        gender = genderSelector[genderSelector.selectedIndex].text;
        dob = document.getElementById("dob").value;
        phoneNumber1 = document.getElementById("phone_number_1").value;
        phoneNumber2 = document.getElementById("phone_number_2").value;
        countrySelector = document.getElementById("country");
        //Getting selected country value
        country = countrySelector[countrySelector.selectedIndex].text;
        state = document.getElementById("state").value;
        city = document.getElementById("city").value;
        suburb = document.getElementById("suburb").value;
        postCode = document.getElementById("post_code").value;
        complaintTypeSelector = document.getElementById("complaint_type");
        //Getting selected complaint type
        complaintType = complaintTypeSelector[complaintTypeSelector.selectedIndex].text;
        complaintDetails = document.getElementById("complaint_details").value;

        firstName.trim();
        lastName.trim();
        email.trim();
        var hasError = false;

        //First Name Validation
        if (firstName.length <= 0) {
            errorFirstName.innerHTML = "Please enter a First Name";
            errorFirstName.style.display = "block";
            document.getElementById('first_name').style.borderColor = "red";
            hasError = true;
        }
        if (containsNumber(firstName)) {
            errorFirstName.innerHTML = "Invalid First Name";
            errorFirstName.style.display = "block";
            document.getElementById('first_name').style.borderColor = "red";
            hasError = true;
        }

        //Last Name Validation
        if (lastName.length <= 0) {
            errorLastName.innerHTML = "Please enter a Last Name";
            errorLastName.style.display = "block";
            document.getElementById('last_name').style.borderColor = "red";
            hasError = true;
        }
        if (containsNumber(lastName)) {
            errorLastName.innerHTML = "Invalid Last Name";
            errorLastName.style.display = "block";
            document.getElementById('last_name').style.borderColor = "red";
            hasError = true;
        }

        //email Validation
        if (!(validateemail(email)) || email.length <= 0) {
            /*if (email === 'n/a') {
                //do nothing
            } else*/ {
                errorEmail.innerHTML = "Please enter a valid email or n/a";
                errorEmail.style.display = "block";
                document.getElementById('email').style.borderColor = "red";
                hasError = true;
            }
        }
        //Gender Validation
        if (gender === 'Select A Gender') {
            document.getElementById('gender').style.borderColor = "red";
            hasError = true;
        }

        //Date Of Birth Validation
        if (!(validateDob(dob))) {
            document.getElementById('dob').style.borderColor = "red";
            hasError = true;
        }

        //Phone Number 1 Validation
        //TODO: ONLY CHECKING LENGTH AND NO ALPHA
        if (phoneNumber1.length > 0) {
            if (phoneNumber1.match(/[a-z]/i) || phoneNumber1.length < 8 || phoneNumber1.length > 13) {
                errorPhone1.innerHTML = "Phone #1 Is Invalid";
                errorPhone1.style.display = "block";
                document.getElementById('phone_number_1').style.borderColor = "red";
                hasError = true;
            }
        }

        //Phone Number 2 Validation
        //todo: ONLY CHECKING LENGTH AND NO ALPHA
        if (phoneNumber2.length > 0) {
            if (phoneNumber2.match(/[a-z]/i) || phoneNumber2.length < 8 || phoneNumber2.length > 13) {
                errorPhone2.innerHTML = "Phone #2 Is Invalid";
                errorPhone2.style.display = "block";
                document.getElementById('phone_number_2').style.borderColor = "red";
                hasError = true;
            }
        }

        //country Validation
        /*if (country === "Select A country") {
            document.getElementById('country').style.borderColor = "red";
            hasError = true;
        }*/

        //state
        if (state.length > 0) {
            if (containsNumber(state)) {
                document.getElementById('state').style.borderColor = "red";
                hasError = true;
            }
        }

        //city
        if (city.length > 0) {
            if (containsNumber(city)) {
                errorCity.innerHTML = "Invalid city";
                errorCity.style.display = "block";
                document.getElementById('city').style.borderColor = "red";
                hasError = true;
            }
        }

        //Suburb
        if (suburb.length > 0) {
            if (containsNumber(suburb)) {
                errorSuburb.innerHTML = "Invalid Suburb";
                errorSuburb.style.display = "block";
                document.getElementById('suburb').style.borderColor = "red";
                hasError = true;
            }
        }

        //Post Code
        //TODO: Imporve this validation
        if (postCode.length > 0) {
            if (postCode.length > 8 || postCode.length < 4) {
                errorPostCode.innerHTML = "Invalid Post Code";
                errorPostCode.style.display = "block";
                document.getElementById('post_code').style.borderColor = "red";
                hasError = true;
            }
        }

        //Complaint Type
        if (complaintType === "Select A Complaint Type") {
            document.getElementById('complaint_type').style.borderColor = "red";
            hasError = true;
        }

        //Complaint Details
        if (complaintDetails.length < 10) {
            errorComplaintDetails.innerHTML = "Please provide some details on the complaint";
            errorComplaintDetails.style.display = "block";
            document.getElementById('complaint_details').style.borderColor = "red";
            hasError = true;
        }

        if (!hasError) {
            return confirmReportCustomer();
        }
        return !hasError;
    }

    //Function for javascript testing
    window.onerror = function (errorMsg, url, lineNumber) {
        alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber);
    };

    //Function to detect number
    function containsNumber(myString) {
        return /\d/.test(myString);
    }

    //Function to validate a email
    function validateemail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


    //function to validate a date
    function validateDob(date) {
        var eighteenYearsAgo = moment().subtract("years", 18);
        var birthday = moment(date);

        if (!birthday.isValid()) {
            return false;
        }
        else if (eighteenYearsAgo.isAfter(birthday)) {
            return true;
        }
        else {
            return false;
        }
    }


</script>
