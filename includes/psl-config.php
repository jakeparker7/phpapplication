<?php
/**
 * These are the database login details
 */  
define("HOST", "localhost");     // The host you want to connect to.
define("USER", "root");    // The database username. 
//define("PASSWORD", "153Karma!");    // The database password.
define("PASSWORD", "");    // The database password.
define("DATABASE", "KarmaDB");    // The database name.
 
define("DEFAULT_ROLE", "member");
 
define("SECURE", FALSE);    // FOR DEVELOPMENT ONLY!!!!
define("KEY_ERROR", "KEY_ERROR");
//Account locked message
define("LOCKED_ACCOUNT_MESSAGE", "Your account has been locked. Please contact support support@guestkarma.com");

//Login Attempt variables
define("LOGIN_ATTEMPTS", "LOGIN_ATTEMPTS");
define("MAX_LOGIN_ATTEMPTS", 4);
define("THRESHOLD_LOGIN_ATTEMPTS", 2);

?>
