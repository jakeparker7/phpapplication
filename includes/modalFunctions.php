<script>
    function confirmAddClient() {
    	hotelName = document.getElementById('hotelName').value;
    	email = document.getElementById('email_add').value;
    	 var html = '<table class="table"><tr>' +
            '<td>Hotel Name</td>' +
            '<th>' + hotelName + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>email</td>' +
            '<th>' + email + '</th>' +
            '</tr>' +
            '</table>';
        var footerHtml = '<div>\n' +
            '<button type="button" class="btn btn-default" style="margin-right: 5px;" onclick="closeDialog();">' +
            'No</button>' +
            '<button type="button" class="btn btn-primary" onclick="submitAddClientemail();">' +
            'Yes <i class="fa fa-spinner fa-spin" id="add-complaint-loading" style="display: none;"></i>\n' +
            '</button>' +
            '</div>';

        showDialog("Confirm", html, footerHtml);
        return false;
    }

    function confirmReportCustomer() {
        firstName = document.getElementById("first_name").value;
        lastName = document.getElementById("last_name").value;
        email = document.getElementById("email").value;
        genderSelector = document.getElementById("gender");
    	//Getting selected Gender value
    	gender = genderSelector[genderSelector.selectedIndex].text;
        dob = document.getElementById("dob").value;
        phoneNumber1 = document.getElementById("phone_number_1").value;
        phoneNumber2 = document.getElementById("phone_number_2").value;
        countrySelector = document.getElementById("country");
    	//Getting selected country value
    	country = countrySelector[countrySelector.selectedIndex].text;
        state = document.getElementById("state").value;
        city = document.getElementById("city").value;
        suburb = document.getElementById("suburb").value;
        postCode = document.getElementById("post_code").value;
        complaintTypeSelector = document.getElementById("complaint_type");
    	//Getting selected complaint type
    	complaintType = complaintTypeSelector[complaintTypeSelector.selectedIndex].text;
        complaintDetails = document.getElementById("complaint_details").value;
        cost = document.getElementById("cost").value;

        var html = '<table class="table"><tr>' +
            '<td>First name</td>' +
            '<th>' + firstName + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Last name</td>' +
            '<th>' + lastName + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>email</td>' +
            '<th>' + email + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Gender</td>' +
            '<th>' + gender + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Date of birth</td>' +
            '<th>' + dob + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Phone 1</td>' +
            '<th>' + phoneNumber1 + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Phone 2</td>' +
            '<th>' + phoneNumber2 + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>country</td>' +
            '<th>' + country + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>state</td>' +
            '<th>' + state + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>city</td>' +
            '<th>' + city + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Suburb</td>' +
            '<th>' + suburb + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Post code</td>' +
            '<th>' + postCode + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Complaint type</td>' +
            '<th>' + complaintType + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Complaint details</td>' +
            '<th>' + complaintDetails + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td>Estimated cost</td>' +
            '<th>' + cost + '</th>' +
            '</tr>' +
            '<tr>' +
            '<td colspan="2">' +
            '<p><input type="checkbox" id="tick" onchange="document.getElementById(\'terms\').disabled = !this.checked;" />' +
            ' I agree to the <a href="terms.html" target="_blank">Terms & Conditions</a></p>' +
            '<td>' +
            '</tr>' +
            '</table>';

        var footerHtml = '<div>\n' +
            '<button type="button" class="btn btn-default" style="margin-right: 5px;" onclick="closeDialog();">' +
            'No</button>' +
            '<button type="button" class="btn btn-primary" onclick="submitReportToDb();" id="terms" disabled>' +
            'Yes <i class="fa fa-spinner fa-spin" id="add-complaint-loading" style="display: none;"></i>\n' +
            '</button>' +
            '</div>';

        showDialog("Confirm", html, footerHtml);
        return false;
    }

    function makeHttpRequest(method, to, callBack, data) {
        var ajax = new XMLHttpRequest();
        method = method.toUpperCase();
        ajax.open(method, to, true);
        if (method == "POST") {
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajax.send(data);
        } else {
            ajax.send();
        }
        ajax.onreadystatechange = function () {
            if (this.statusText === "OK") {
                callBack(this.responseText);
            }
        }
    }

    function onSearch() {
        $("#search-loading").show();

        var firstName = $("#search_first_name").val();
        var lastName = $("#search_last_name").val();
        var dob = $("#search_dob").val();

        $.ajax({
            url: "http.php",
            method: "POST",
            data: {search: 1, firstName: firstName, lastName: lastName, dob: dob},
            success: function (response) {
                $("#search-loading").hide();
                console.log(response);

                var data = JSON.parse(response);
                console.log(data);

                if (data.error == "") {
                    if (data.complaints.length === 0) {
                        showDialog("Search", "No record found", "<button onclick='closeDialog();'>Try again</button>");
                    } else {
                        var html = "<table class='table'>";
                        var personData = data.complaints[0];

                        html += "<tr><th>Person</th></tr>";

                        html += "<tr><td>First name</td> <td>" + personData.first_name + "</td></tr>";
                        html += "<tr><td>Last name</td> <td>" + personData.surname + "</td></tr>";
                        html += "<tr><td>DOB</td> <td>" + personData.dob + "</td></tr>";
                        html += "<tr><td>email</td> <td>" + personData.email + "</td></tr>";

                        html += "<tr><td>Phone Number 1</td> <td>" + personData.phone_number_1 + "</td></tr>";
                        html += "<tr><td>Phone Number 2</td> <td>" + personData.phone_number_2 + "</td></tr>";

                        html += "<tr><td>city</td> <td>" + personData.city + "</td></tr>";
                        html += "<tr><td>state</td> <td>" + personData.state + "</td></tr>";
                        html += "<tr><td>country</td> <td>" + personData.country + "</td></tr>";

                        for (var a = 0; a < data.complaints.length; a++) {
                            html += "<tr><td style='font-weight: bold;'>Complaint ID " + data.complaints[a].complaint_id + "</td></tr>";

                            html += "<tr>";
                            html += "<td>Complaint type</td>";
                            html += "<td>" + data.complaints[a].complaint_type + "</td>";
                            html += "</tr>";

                            html += "<tr>";
                            html += "<td>Complaint description</td>";
                            html += "<td>" + data.complaints[a].complaint_description + "</td>";
                            html += "</tr>";

                            html += "<tr>";
                            html += "<td>Complaint at</td>";
                            html += "<td>" + data.complaints[a].time_added + "</td>";
                            html += "</tr>";
                        }
                        html += "</table>";

                        showDialog("Record found", html, "<button onclick='closeDialog();'>OK</button>");
                    }
                } else {
                    showDialog("Error", data.error, "<button onclick='closeDialog();'>Try again</button>");
                }
            }
        });

        return false;
    }

    function showDialog(header, body, footer) {
        $("#white-background").show();
        $("#dlgbox").show();

        var winWidth = window.innerWidth;
        var winHeight = window.innerHeight;

        $("#dlgbox").css({
            left: (winWidth / 2) - (480 / 2) + "px",
            top: "150px"
        });

        $("#dlg-header").html(header);
        $("#dlg-body").html(body);
        $("#dlg-footer").html(footer);
    }

    function closeDialog() {
        $("#white-background").hide();
        $("#dlgbox").hide();
    }
</script>