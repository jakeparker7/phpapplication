<?php

require_once 'includes/db_connect.php';
require_once 'includes/functions.php';
require_once 'includes/header.php';

if (isset($_GET["createPassword"])) {
    $email = $_GET["email"];
    $token = $_GET["token"];

    $sql = "SELECT * FROM clients WHERE email = ? AND token = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("ss", $email, $token);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $member = $result->fetch_object();
        $dif = time() - $member->mailed_at;

        if ($dif > 86400) {
            echo "<p style='text-align: center;
    margin-top: 10%;
    font-size: 20px;
    color: red;
    font-weight: bold;'>This link has been expired</p>";
        } else { ?>
            <div class="container">
                <div class="c-box2">
                    <p class="title2">Create password</p>
                    <hr class="c-brdr1">
                    <div class="form c-form">
                        <form method="post" onsubmit="return validateFormData(this);">
                            <div class="col-lg-12 p-0">
                                <div class="col-lg-4 p-0">
                                    <div class="p-r-15">
                                        <label for="">Password</label> <input class="form-control cstm-form-control"
                                                                              name="password" type="password"
                                                                              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}.\W"
                                                                              title="Must contain at least one number and one uppercase and lowercase letter, and at least 5 characters and a special character"
                                                                              required>
                                    </div>
                                </div>
                                <div class="col-lg-4 p-0">
                                    <div class="p-r-15">
                                        <label for="">Confirm passwrd</label> <input
                                                class="form-control cstm-form-control"
                                                name="confirm_password" type="password"
                                                required>
                                    </div>
                                </div>
                                <button type="submit" name="submit" class="cstm-btn"
                                        style="margin-top: 29px; padding: 10px 35px;">
                                    Set password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
            require_once 'includes/footer.php';
            require_once 'includes/modalFunctions.php';
            require_once 'includes/dbFunctions.php';

        }
    } else {
        echo "You are not authorized for this page";
    }
} else {
    echo "You are not authorized for this page";
}

?>

<script>
    function validateFormData(form) {
        if (form.password.value !== form.confirm_password.value) {
            showDialog("Error", "Password does not match", "<button onclick='closeDialog();'>Try again</button>");
        } else {
            var italic = document.createElement("i");
            italic.setAttribute("class", "fa fa-spinner fa-spin");
            italic.style.marginLeft = "5px";
            form.submit.appendChild(italic);

            $.ajax({
                url: "http.php",
                method: "POST",
                data: {
                    createPassword: 1,
                    email: "<?= $email; ?>",
                    token: "<?= $token; ?>",
                    password: form.password.value
                },
                success: function (response) {
                    console.log(response);
                    form.submit.removeChild(italic);

                    showDialog("Account created", "Please login to continue", "<button onclick='window.location.href = \"includes/logout.php\"'>Login</button>");
                }
            });
        }
        return false;
    }
</script>
