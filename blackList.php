<?php

include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
require_once 'includes/header.php';

if (!isset($_SESSION['client_id'])) {
    header("Location: index.php");
}

?>

<?php if (login_check($mysqli) == true) : ?>

    <div class="clearfix"></div>
    <div class="c-box1">
        <div class="col-lg-12">
            <div class="col-lg-offset-4 col-lg-4">
                <p class="title1">Black List</p><br>
                <hr>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="container">
        <div class="c-cont1">
            <p>
                This is a community that beleives in Karma. Disrespectful occupants should be held accountable for their
                actions.
                <br><br>
                Please ensure all information added is complete and correct to maximise the value of the community.
                <br><br>
                If you would like any new features or a discrepancy is found, e-mail us at support@guestkarma.com.
                <br><br>
                <b>The Guest Karma team</b>
            </p>
        </div>
        <div class="c-box2">
            <p class="title2">Upload A File</p>
            <hr class="c-brdr1">
            <div class="form c-form">
                <form action="upload.php" method="post" enctype="multipart/form-data"
                      onsubmit="return uploadBlackList();">
                    <p>Do you have a spread sheet or CSV containing information of Bad Guests?
                        <br>
                        Upload it and the Guest Karma team will add it to the Black List for you.</p>
                    <br>
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <br>
                    <button type="submit" name="submit" class="cstm-btn" id="upload-btn">
                        Upload File
                    </button>
                </form>
            </div>
        </div>

        <script>
            function uploadBlackList() {
                if ($('#fileToUpload')[0].files.length == 0) {
                    showDialog("Error", "Please select a file first", "<button onclick='closeDialog();'>OK</button>");
                } else {
                    var button = document.getElementById("upload-btn");
                    var italic = document.createElement("i");
                    italic.setAttribute("class", "fa fa-spinner fa-spin");
                    italic.style.marginLeft = "5px";
                    button.appendChild(italic);

                    var formData = new FormData();
                    formData.append('fileToUpload', $('#fileToUpload')[0].files[0]);

                    $.ajax({
                        url: 'upload.php',
                        type: 'POST',
                        data: formData,
                        processData: false,  // tell jQuery not to process the data
                        contentType: false,  // tell jQuery not to set contentType
                        success: function (data) {
                            button.removeChild(italic);
                            console.log(data);
                            alert(data);
                        }
                    });
                }
                return false;
            }
        </script>

        <div class="c-box2">
            <p class="title2">Search Black List</p>
            <hr class="c-brdr1">
            <div class="form c-form">
                <form action="/search_black_list.php" id="searchForm" method="post" name="searchForm"
                      onsubmit="return onSearch();">
                    <div class="col-lg-12 p-0" style="padding: 0px;">
                        <div class="col-lg-4 p-0" style="padding: 0px;">
                            <div class="p-r-15">
                                <label for="">First Name</label>
                                <input type="text" class="form-control cstm-form-control" id="search_first_name"
                                       name="first_name" required>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0" style="padding: 0px;">
                            <div class="p-r-15">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control cstm-form-control" id="search_last_name"
                                       name="last_name" required>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0" style="padding: 0px;">
                            <label for="">Date Of Birth</label>
                            <input type="date" class="form-control cstm-form-control" id="search_dob" name="dob" required
                            >
                        </div>
                        <br><br>
                        <p>By clicking search below you are acknowledging that the person in question has consented to
                            having their name checked against the Guest Karma Database (under Australian Privacy
                            Principles).</p>
                        <!--<p>By clicking Search you are agreeing to the <a href= "www.guestkarma.com/pages/terms" >Term's & Conditions </a></p> -->
                        <button type="submit" class="cstm-btn" style="margin: 20px 0px;">
                            Search <i class="fa fa-spinner fa-spin" id="search-loading" style="display: none;"></i>
                        </button>
                        <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <div class="c-box2">
            <p class="title2">Report A Customer</p>
            <hr class="c-brdr1">
            <div class="form c-form">
                <form id="reportForm" method="post" name="reportForm"
                      onsubmit="return validateReport();">

                    <div class="col-lg-12 p-0">
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">First Name</label>
                                <input type="text" class="form-control cstm-form-control" id="first_name"
                                       name="first_name">
                                <span class="error" id="errorFirstName"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control cstm-form-control" id="last_name"
                                       name="last_name">
                                <span class="error" id="errorLastName"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Date Of Birth</label>
                                <input type="date" class="form-control cstm-form-control" id="dob" name="dob" />
                                <span class="error" id="errorDate"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Email</label>
                                <input type="text" class="form-control cstm-form-control" id="email" name="email"/>
                                <span class="error" id="errorEmail"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Gender</label>
                                <select name="gender" class="form-control cstm-form-control" id="gender">
                                    <option value="NULL" selected="selected">Select A Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Phone #1</label>
                                <input type="text" class="form-control cstm-form-control" id="phone_number_1"
                                       name="phone_number_1" />
                                <span class="error" id="errorPhone1"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Phone #2</label>
                                <input type="text" class="form-control cstm-form-control" id="phone_number_2"
                                       name="phone_number_2" />
                                <span class="error" id="errorPhone2"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Country</label>
                                <select class="form-control cstm-form-control" id="country" name="country"
                                        onchange="oncountrySelected();">
                                    <option value="NULL" selected="selected"></option>
                                    <?php
                                    $stmt = $mysqli->prepare("SELECT id, name FROM countries ORDER BY name ASC");
                                    $stmt->execute();
                                    $result = $stmt->get_result();
                                    while ($name = $result->fetch_assoc()) {
                                        echo "<option value ='" . $name["id"] . "'>" . $name["name"] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">State</label>
                                <select class="form-control cstm-form-control" id="state" name="state">
                                    <option value="NULL" selected="selected"></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">City</label>
                                <input type="text" class="form-control cstm-form-control" id="city" name="city" />
                                <span class="error" id="errorCity"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Suburb</label>
                                <input type="text" class="form-control cstm-form-control" id="suburb" name="suburb" />
                                <span class="error" id="errorSuburb"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Post Code</label>
                                <input type="text" class="form-control cstm-form-control" id="post_code"
                                       name="post_code" />
                                <span class="error" id="errorPostCode"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Complaint Type</label>
                                <select name="complaint_type" class="form-control cstm-form-control"
                                        id="complaint_type">
                                    <option value="NULL" selected="selected">Select A Complaint Type</option>
                                    <option value="violence">Violence</option>
                                    <option value="damage">Damage</option>
                                    <option value="no_pay">Did Not Pay</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 p-0">
                            <div class="p-r-15">
                                <label for="">Estimated Cost</label>
                                <input name="cost" type="number" class="form-control cstm-form-control" id="cost" min="1" max="1000000" />
                                <span class="error" id="errorCost"></span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                        <label for="">Complaint Details</label><br>
                        <textarea name="complaint_details" id="complaint_details" cols="30" rows="8"
                                  class="w-100" minlength="10"></textarea>
                        <span class="error" id="errorComplaintDetails"></span>
                        <br><br><br>
                        <button type="submit" class="cstm-btn" style="margin-bottom: 100px;">
                            Send
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>

    </div>

    </div>

    <?php require_once 'includes/footer.php'; ?>
    <?php require_once 'includes/modalFunctions.php'; ?>
    <?php require_once 'includes/dbFunctions.php'; ?>
    <?php require_once 'includes/validations.php'; ?>

<?php else :
    header('Location: ../index.php');
endif; ?>

<script>
    function oncountrySelected() {
        var countryCode = document.getElementById("country").value;
        $.ajax({
            url: "http.php",
            method: "POST",
            data: {getstates: 1, countryCode: countryCode},
            success: function (response) {
//                console.log(response);

                var data = JSON.parse(response);
                var html = "";
                html += "<option value='NULL' selected='selected'></option>";

                for (var a = 0; a < data.length; a++) {
                    html += "<option value='" + data[a].name + "'>" + data[a].name + "</option>";
                }

//                if (data.length === 0) {
//                    html += "<option value='noSelect' selected='selected'>Select A state</option>";
//                }

                document.getElementById("state").innerHTML = html;
            }
        });
    }
</script>

</body>
</html>
