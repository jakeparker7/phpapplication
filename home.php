<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
require 'PHPMailer/PHPMailerAutoload.php';

sec_session_start();
require_once 'includes/header.php';

if (!isset($_SESSION['client_id'])) {
    header("Location: index.php");
}

?>

<?php if (login_check($mysqli) == true) : ?>

    <div class="clearfix"></div>
    <div class="c-box1">
        <div class="col-lg-12">
            <div class="col-lg-offset-4 col-lg-4">
                <p class="title1">Welcome, <span><?php echo $_SESSION['username']; ?></span></p><br>
                <hr>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="container">
        <div class="c-cont1">
            <p class="title2">Statistics</p>
            <hr class="c-brdr1">
            <p>
                <br>
                

                <?php $client_id = $_SESSION['client_id'];
                    //echo "client id" . $client_id;


                    $stmt = $mysqli->prepare("SELECT reports_made, customers_checked, bad_guests_detected, money_saved FROM client_statistics WHERE client_id = ? LIMIT 1");
                    $stmt->bind_param('i', $client_id);
                    $stmt->execute();
                    $stmt->store_result();
                    $stmt->bind_result($reports_made, $customers_checked, $bad_guests_detected, $money_saved);
                    $stmt->fetch();

                    echo '<b>Reports Made: </b>' . $reports_made . '<br>';
                    echo '<b>Customers Checked: </b>' . $customers_checked . '<br>';
                    echo '<b>Bad Guests Detected: </b>' . $bad_guests_detected . '<br>';

                    ?>
                <br>
                <!--<b>Reports Made: </b> 5<br>
                <b>Customers Checked: </b> 2587<br>
                <b>Bad Guests Detected: </b> 8-->
            </p>
        </div>
        <div class="c-cont1">
            <p class="title2">Report Of The Week</p>
            <hr class="c-brdr1">
            <p>
                <b>Report By: </b>ILab<br>
                <b>Complaint Type: </b>Damage<br>
                <b>Complaint Details: </b>A regular resident and Gold Coast local Sam Small has been discovered running
                a brothel inside one of our rooms. Sam has facilitated multiple organised homosexual orgys and was found
                in the possesion of large amounts of cocaine.
            </p>
        </div>

        <?php
        $client_id = $_SESSION['client_id'];
        $sql = "SELECT complaints.time_added, complaints.complaint_type, complaints.person_id, person.first_name, person.surname  FROM complaints INNER JOIN person ON complaints.person_id = person.person_id WHERE person.client_id = ? ORDER BY time_added DESC LIMIT 30";

        //echo "<p>$sql</p>";
        //echo "<p>$client_id</p>";

        $stmt = $mysqli->prepare($sql);

        $stmt->bind_param('s', $client_id);
        $stmt->execute();
        $result = $stmt->get_result();
        $num_of_rows = $result->num_rows;
        ?>

        <div class="c-box2">
            <p class="title2">My Reports</p>
            <hr class="c-brdr1">
            <div class="form c-form">
                <br>
                <table class="table">
                    <tr>
                        <th>Date Added</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Complaint Type</th>
                    </tr>

                    <?php
                    if ($num_of_rows > 0) {
                        while ($row = $result->fetch_object()) {
                            ?>
                            <tr>
                                <td><?= $row->time_added; ?></td>
                                <td><?= $row->first_name; ?></td>
                                <td><?= $row->surname; ?></td>
                                <td><?= $row->complaint_type; ?></td>
                            </tr>
                        <?php
                        }
                    } else {
                        echo '<tr><td colspan="4" align="center" style="border-bottom: 1px solid #ddd;">No Result found</td></tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    
    <?php if($_SESSION['admin'] == 1) {
        require_once 'admin.php';
    }?>
    </div>
    <?php require_once 'includes/footer.php'; ?>
    <?php require_once 'includes/modalFunctions.php'; ?>
    <?php require_once 'includes/dbFunctions.php'; ?>
  </body>
</html>
<?php else :
    header('Location: index.php');
endif; ?>
